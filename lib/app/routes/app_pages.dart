import 'package:get/get.dart';

import 'package:cash_management/app/modules/account/bindings/account_binding.dart';
import 'package:cash_management/app/modules/account/views/account_view.dart';
import 'package:cash_management/app/modules/budget/bindings/budget_binding.dart';
import 'package:cash_management/app/modules/budget/views/budget_view.dart';
import 'package:cash_management/app/modules/home/bindings/home_binding.dart';
import 'package:cash_management/app/modules/home/views/home_view.dart';
import 'package:cash_management/app/modules/splash/bindings/splash_binding.dart';
import 'package:cash_management/app/modules/splash/views/splash_view.dart';
import 'package:cash_management/app/modules/transaction/bindings/transaction_binding.dart';
import 'package:cash_management/app/modules/transaction/views/transaction_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.HOME;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.ACCOUNT,
      page: () => AccountView(),
      binding: AccountBinding(),
    ),
    GetPage(
      name: _Paths.TRANSACTION,
      page: () => TransactionView(),
      binding: TransactionBinding(),
    ),
    GetPage(
      name: _Paths.BUDGET,
      page: () => BudgetView(),
      binding: BudgetBinding(),
    ),
    GetPage(
      name: _Paths.SPLASH,
      page: () => SplashView(),
      binding: SplashBinding(),
    ),
  ];
}
