part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();

  static const HOME = _Paths.HOME;
  static const ACCOUNT = _Paths.ACCOUNT;
  static const TRANSACTION = _Paths.TRANSACTION;
  static const BUDGET = _Paths.BUDGET;
  static const SPLASH = _Paths.SPLASH;
}

abstract class _Paths {
  static const HOME = '/home';
  static const ACCOUNT = '/account';
  static const TRANSACTION = '/transaction';
  static const BUDGET = '/budget';
  static const SPLASH = '/splash';
}
