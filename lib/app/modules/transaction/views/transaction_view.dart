import 'package:cash_management/app/modules/transaction/views/add_transaction_view.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';

import '../../../widgets/card_list_item.dart';
import '../controllers/transaction_controller.dart';

class TransactionView extends GetView<TransactionController> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(Get.height * 0.5),
          child: SizedBox(
            child: Column(
              children: [
                AppBar(
                  leading: Icon(Icons.arrow_back_ios, color: Colors.black, size: 20),
                  backgroundColor: Colors.white,
                  title: GestureDetector(
                    onTap: (){
                      Get.to(AddTransactionView());
                    },
                    child: Text(
                      'Statistics',
                      style: GoogleFonts.inter(
                        color: Colors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  centerTitle: true,
                  actions: [],
                ),
                SizedBox(
                  height: 50,
                  child: Row(
                    children: [
                      Expanded(
                        flex: 1,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            decoration: BoxDecoration(color: Colors.teal, borderRadius: BorderRadius.circular(20.0)),
                            child: Center(
                              child: Text(
                                'Day',
                                style: GoogleFonts.inter(
                                  color: Colors.black,
                                  fontSize: 13,
                                  fontWeight: FontWeight.w300,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            decoration: BoxDecoration(color: Colors.teal, borderRadius: BorderRadius.circular(20.0)),
                            child: Center(
                              child: Text(
                                'Week',
                                style: GoogleFonts.inter(
                                  color: Colors.black,
                                  fontSize: 13,
                                  fontWeight: FontWeight.w300,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            decoration: BoxDecoration(color: Colors.teal, borderRadius: BorderRadius.circular(20.0)),
                            child: Center(
                              child: Text(
                                'Month',
                                style: GoogleFonts.inter(
                                  color: Colors.black,
                                  fontSize: 13,
                                  fontWeight: FontWeight.w300,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            decoration: BoxDecoration(color: Colors.teal, borderRadius: BorderRadius.circular(20.0)),
                            child: Center(
                              child: Text(
                                'Year',
                                style: GoogleFonts.inter(
                                  color: Colors.black,
                                  fontSize: 13,
                                  fontWeight: FontWeight.w300,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 40, child: Text(('Expense income tab'))),
                Container(
                  color: Colors.teal,
                  height: 200.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        'Top Spending',
                        style: GoogleFonts.inter(
                          color: Colors.black,
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Icon(Icons.description),
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Card(
                color: HexColor('FBFBFB'),
                  elevation: 0,
                  child: CardListItem(title: 'Persembahan mirunggan', date: 'Today', nominal: '200', status: '+')),
              Card(
                color: HexColor('FBFBFB'),
                  elevation: 0,
                  child: CardListItem(title: 'Persembahan mirunggan', date: 'Today', nominal: '200', status: '+')),
              Card(
                  color: HexColor('FBFBFB'),
                  elevation: 0,
                  child: CardListItem(title: 'Persembahan mirunggan', date: 'Today', nominal: '200', status: '+')),
              Card(
                  color: HexColor('FBFBFB'),
                  elevation: 0,
                  child: CardListItem(title: 'Persembahan mirunggan', date: 'Today', nominal: '200', status: '+')),
              Card(
                  color: HexColor('FBFBFB'),
                  elevation: 0,
                  child: CardListItem(title: 'Persembahan mirunggan', date: 'Today', nominal: '200', status: '+')),
              Card(
                  color: HexColor('FBFBFB'),
                  elevation: 0,
                  child: CardListItem(title: 'Persembahan mirunggan', date: 'Today', nominal: '200', status: '+')),
              Card(
                  color: HexColor('FBFBFB'),
                  elevation: 0,
                  child: CardListItem(title: 'Persembahan mirunggan', date: 'Today', nominal: '200', status: '+')),
              Card(
                  color: HexColor('FBFBFB'),
                  elevation: 0,
                  child: CardListItem(title: 'Persembahan mirunggan', date: 'Today', nominal: '200', status: '+')),
              Card(
                  color: HexColor('FBFBFB'),
                  elevation: 0,
                  child: CardListItem(title: 'Persembahan mirunggan', date: 'Today', nominal: '200', status: '+')),
              Card(
                  color: HexColor('FBFBFB'),
                  elevation: 0,
                  child: CardListItem(title: 'Persembahan mirunggan', date: 'Today', nominal: '200', status: '+')),
            ],
          ),
        ),
      ),
    );
  }
}
