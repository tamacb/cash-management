import 'package:badges/badges.dart';
import 'package:cash_management/app/modules/transaction/views/transaction_view.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';

import '../../../widgets/card_list_item.dart';
import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: PreferredSize(
          child: Container(
              color: Colors.grey[100],
              child: Stack(
                children: [
                  Column(
                    children: [
                      Stack(
                        children: [
                          Image.asset('assets/appbarbg.png'),
                          Image.asset('assets/appbarpop.png', height: 300),
                        ],
                      ),
                      SizedBox(
                        height: 10.0,
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(35.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Text(
                                    'Good afternoon',
                                    style: GoogleFonts.inter(
                                      color: Colors.white,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Text(
                                    'Mas Tama',
                                    style: GoogleFonts.inter(
                                      color: Colors.white,
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Card(
                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
                                color: Colors.white.withOpacity(0.1),
                                child: IconButton(
                                    onPressed: () {},
                                    icon: Badge(
                                        badgeColor: Colors.orange,
                                        child: const Icon(
                                          Icons.notifications_rounded,
                                          color: Colors.white,
                                        )))),
                          ],
                        ),
                      ),
                      Flexible(
                        child: Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15.0),
                              color: HexColor('2F7E79'),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(15.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'Total Balance',
                                            style: GoogleFonts.inter(
                                              color: Colors.white,
                                              fontSize: 16,
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                          Text(
                                            '\$ 2.500.00',
                                            style: GoogleFonts.inter(
                                              color: Colors.white,
                                              fontSize: 30,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ],
                                      ),
                                      const Icon(
                                        Icons.more_horiz,
                                        color: Colors.white,
                                      )
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(15.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Column(
                                        children: [
                                          Row(
                                            children: [
                                              const Icon(Icons.arrow_downward, color: Colors.lightGreenAccent),
                                              Text(
                                                'income',
                                                style: GoogleFonts.inter(
                                                  color: Colors.white,
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w300,
                                                ),
                                              ),
                                            ],
                                          ),
                                          Text(
                                            '\$ 1.200',
                                            style: GoogleFonts.inter(
                                              color: Colors.white,
                                              fontSize: 20,
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                        ],
                                      ),
                                      Column(
                                        children: [
                                          Row(
                                            children: [
                                              Icon(Icons.arrow_upward, color: Colors.red),
                                              Text(
                                                'expense',
                                                style: GoogleFonts.inter(
                                                  color: Colors.white,
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w300,
                                                ),
                                              ),
                                            ],
                                          ),
                                          Text(
                                            '\$ 1.200',
                                            style: GoogleFonts.inter(
                                              color: Colors.white,
                                              fontSize: 20,
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      )
                    ],
                  )
                ],
              )),
          preferredSize: Size.fromHeight(Get.height * 0.4),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Text(
                      'Transactions History',
                      style: GoogleFonts.inter(
                        color: Colors.black,
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Text(
                      'See all',
                      style: GoogleFonts.inter(
                        color: Colors.black,
                        fontSize: 14,
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                  ),
                ],
              ),
              CardListItem(title: 'Persembahan mirunggan', date: 'Today', nominal: '200', status: '+'),
              CardListItem(title: 'Service AC', date: 'Today', nominal: '50', status: '-'),
              CardListItem(title: 'Persembahan mirunggan', date: 'Today', nominal: '200', status: '+'),
              CardListItem(title: 'Service AC', date: 'Today', nominal: '50', status: '-'),
              CardListItem(title: 'Persembahan mirunggan', date: 'Today', nominal: '200', status: '+'),
              CardListItem(title: 'Service AC', date: 'Today', nominal: '50', status: '-'),
              CardListItem(title: 'Persembahan mirunggan', date: 'Today', nominal: '200', status: '+'),
              CardListItem(title: 'Service AC', date: 'Today', nominal: '50', status: '-'),
              CardListItem(title: 'Persembahan mirunggan', date: 'Today', nominal: '200', status: '+'),
              CardListItem(title: 'Service AC', date: 'Today', nominal: '50', status: '-'),
              CardListItem(title: 'Persembahan mirunggan', date: 'Today', nominal: '200', status: '+'),
              CardListItem(title: 'Service AC', date: 'Today', nominal: '50', status: '-'),
              CardListItem(title: 'Persembahan mirunggan', date: 'Today', nominal: '200', status: '+'),
              CardListItem(title: 'Service AC', date: 'Today', nominal: '50', status: '-'),
              CardListItem(title: 'Persembahan mirunggan', date: 'Today', nominal: '200', status: '+'),
              CardListItem(title: 'Service AC', date: 'Today', nominal: '50', status: '-'),
            ],
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        bottomNavigationBar: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 15.0),
              child: Image.asset('assets/home.png', width: 35, height: 35),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 15.0),
              child: Image.asset('assets/transactions.png', width: 35, height: 35),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 10.0),
              child:   CircleAvatar(child: IconButton(onPressed: (){
                Get.to(TransactionView());
              },icon: const Icon(Icons.add),)),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 15.0),
              child: Image.asset('assets/wallet.png', width: 35, height: 35),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 15.0),
              child: Image.asset('assets/user.png', width: 35, height: 35),
            ),
          ],
        ),
      ),
    );
  }
}
