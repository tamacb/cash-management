import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CardListItem extends StatelessWidget {
  String? title;
  String? nominal;
  String? date;
  String? status;

  CardListItem({Key? key, this.title, this.date, this.nominal, this.status}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
        title!,
        style: GoogleFonts.inter(
          color: Colors.black,
          fontSize: 16,
          fontWeight: FontWeight.normal,
        ),
      ),
      subtitle: Text(
        date!,
        style: GoogleFonts.inter(
          color: Colors.black,
          fontSize: 13,
          fontWeight: FontWeight.w300,
        ),
      ),
      leading: (status == "+")
          ? Icon(Icons.arrow_downward_sharp, color: Colors.green)
          : Icon(Icons.arrow_upward_sharp, color: Colors.red),
      trailing: Text(
        '$status \$ $nominal',
        style: GoogleFonts.inter(
          color: (status == "+") ? Colors.green : Colors.red,
          fontSize: 18,
          fontWeight: FontWeight.w600,
        ),
      ),
    );
  }
}
